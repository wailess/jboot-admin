package io.jboot.admin.base.plugin.shiro;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.servlet.OncePerRequestFilter;

import com.jfinal.core.Const;


/**
 * shiro 编码过滤器
 * @author Rlax
 *
 */
public class CharacterEncodingFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        request.setCharacterEncoding(Const.DEFAULT_ENCODING);
        chain.doFilter(request, response);
    }
}
