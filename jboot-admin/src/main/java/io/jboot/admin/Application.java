package io.jboot.admin;

import io.jboot.app.JbootApplication;

/**
 * 服务启动入口
 * @author Rlax
 *
 */
public class Application {
    public static void main(String [] args){
        JbootApplication.run(args);
    }
}
