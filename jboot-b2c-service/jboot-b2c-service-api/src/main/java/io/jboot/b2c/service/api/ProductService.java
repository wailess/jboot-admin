package io.jboot.b2c.service.api;

import com.jfinal.plugin.activerecord.Page;

import io.jboot.b2c.service.entity.model.Product;

public interface ProductService  {

    /**
     * 分页查询商品信息
     * @param product
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Product> findPage(Product product, int pageNumber, int pageSize);

    /**
     * 根据ID查找model
     *
     * @param id
     * @return
     */
    public Product findById(Object id);


    /**
     * 根据ID删除model
     *
     * @param id
     * @return
     */
    public boolean deleteById(Object id);

    /**
     * 删除
     *
     * @param model
     * @return
     */
    public boolean delete(Product model);


    /**
     * 保存到数据库
     *
     * @param model
     * @return
     */
    public Object save(Product model);

    /**
     * 保存或更新
     *
     * @param model
     * @return
     */
    public Object saveOrUpdate(Product model);

    /**
     * 更新 model
     *
     * @param model
     * @return
     */
    public boolean update(Product model);
}