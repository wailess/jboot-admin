package io.jboot.admin.service.entity.model;

import io.jboot.admin.service.entity.model.base.BaseUser;
import io.jboot.db.annotation.Table;

/**
 * Generated by Jboot.
 */
@Table(tableName = "sys_user", primaryKey = "id")
public class User extends BaseUser<User> {
	
}
