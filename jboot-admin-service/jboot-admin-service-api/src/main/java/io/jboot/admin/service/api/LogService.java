package io.jboot.admin.service.api;

import com.jfinal.plugin.activerecord.Page;

import io.jboot.admin.service.entity.model.Log;

public interface LogService  {

    /**
     * 分页查询数据字典信息
     * @param log
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Log> findPage(Log log, int pageNumber, int pageSize);

    /**
     * 根据ID查找model
     *
     * @param id
     * @return
     */
    public Log findById(Object id);


    /**
     * 根据ID删除model
     *
     * @param id
     * @return
     */
    public boolean deleteById(Object id);

    /**
     * 删除
     *
     * @param model
     * @return
     */
    public boolean delete(Log model);


    /**
     * 保存到数据库
     *
     * @param model
     * @return
     */
    public Object save(Log model);

    /**
     * 保存或更新
     *
     * @param model
     * @return
     */
    public Object saveOrUpdate(Log model);

    /**
     * 更新 model
     *
     * @param model
     * @return
     */
    public boolean update(Log model);

}