package io.jboot.admin.service.api;

import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Page;

import io.jboot.admin.service.entity.model.Data;

public interface DataService  {
    
    /**
     * 分页查询数据字典信息
     * @param data
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Data> findPage(Data data, int pageNumber, int pageSize);

    
    /**
     * 查询字典表 通过类型与code,type获取对应描述
     * @param code
     * @param type
     * @return
     */
    public String getCodeDescByCodeAndType(String code, String type);

    /**
     * 查询字典表 通过类型 与 描述 , 获取code
     * @param type 数据类型
     * @param codeDesc 描述
     * @return
     */
    public String getCodeByCodeDescAndType(String type, String codeDesc);

    /**
     * 查询字典表 通过类型代码 , 获取map
     * @param type
     * @return
     */
    public Map<String, String> getMapByTypeOnUse(String type);

    /**
     * 查询字典表 通过类型代码 、状态, 获取map
     * @param type 数据类型
     * @return
     */
    public Map<String, String> getMapByType(String type);

    /**
     * 根据 type/status 获取数据字典列表
     * @param type 类型编码
     * @return
     */
    public List<Data> getListByTypeOnUse(String type);

    /**
     * 根据 type 获取全部数据字典列表
     * @param type
     * @return
     */
    public List<Data> getListByType(String type);

    /**
     * 刷新缓存
     */
    public void refreshCache();

    /**
     * 根据ID查找model
     *
     * @param id
     * @return
     */
    public Data findById(Object id);


    /**
     * 根据ID删除model
     *
     * @param id
     * @return
     */
    public boolean deleteById(Object id);

    /**
     * 删除
     *
     * @param model
     * @return
     */
    public boolean delete(Data model);


    /**
     * 保存到数据库
     *
     * @param model
     * @return
     */
    public Object save(Data model);

    /**
     * 保存或更新
     *
     * @param model
     * @return
     */
    public Object saveOrUpdate(Data model);

    /**
     * 更新 model
     *
     * @param model
     * @return
     */
    public boolean update(Data model);
}