package io.jboot.admin.service.api;

import java.util.List;

import io.jboot.admin.service.entity.model.RoleRes;

public interface RoleResService  {

    /**
     * 根据roleId删除model
     * @param roleId
     */
    public int deleteByRoleId(Long roleId);

    /**
     * 批量保存 model
     * @param list
     * @return
     */
    public int[] batchSave(List<RoleRes> list);

    /**
     * 根据ID查找model
     *
     * @param id
     * @return
     */
    public RoleRes findById(Object id);


    /**
     * 根据ID删除model
     *
     * @param id
     * @return
     */
    public boolean deleteById(Object id);

    /**
     * 删除
     *
     * @param model
     * @return
     */
    public boolean delete(RoleRes model);


    /**
     * 保存到数据库
     *
     * @param model
     * @return
     */
    public Object save(RoleRes model);

    /**
     * 保存或更新
     *
     * @param model
     * @return
     */
    public Object saveOrUpdate(RoleRes model);

    /**
     * 更新 model
     *
     * @param model
     * @return
     */
    public boolean update(RoleRes model);
}