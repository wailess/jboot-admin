package io.jboot.admin.service.api;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;

import io.jboot.admin.service.entity.model.Role;

public interface RoleService  {

    /**
     * 分页查询角色信息
     * @param sysRole
     * @return
     */
    public Page<Role> findPage(Role sysRole, int pageNumber, int pageSize);

    /**
     * 根据用户名查询用户所具有的角色列表信息
     * @param name
     * @return
     */
    public List<Role> findByUserName(String name);

    /**
     * 角色赋权
     * @param id 角色id
     * @param resIds 角色资源树 ids
     */
    public boolean auth(Long id, String resIds);

    /**
     * 查询可用角色列表
     * @return
     */
    public List<Role> findByStatusUsed();
    
    /**
     * 根据ID查找model
     *
     * @param id
     * @return
     */
    public Role findById(Object id);


    /**
     * 根据ID删除model
     *
     * @param id
     * @return
     */
    public boolean deleteById(Object id);

    /**
     * 删除
     *
     * @param model
     * @return
     */
    public boolean delete(Role model);


    /**
     * 保存到数据库
     *
     * @param model
     * @return
     */
    public Object save(Role model);

    /**
     * 保存或更新
     *
     * @param model
     * @return
     */
    public Object saveOrUpdate(Role model);

    /**
     * 更新 model
     *
     * @param model
     * @return
     */
    public boolean update(Role model);

}