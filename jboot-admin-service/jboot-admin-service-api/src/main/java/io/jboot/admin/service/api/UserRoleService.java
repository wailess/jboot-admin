package io.jboot.admin.service.api;

import java.util.List;

import io.jboot.admin.service.entity.model.UserRole;

public interface UserRoleService  {

    /**
     * 根据userId删除model
     * @param userId
     */
    public int deleteByUserId(Long userId);

    /**
     * 批量保存 model
     * @param list
     * @return
     */
    public int[] batchSave(List<UserRole> list);

    /**
     * 根据ID查找model
     *
     * @param id
     * @return
     */
    public UserRole findById(Object id);


    /**
     * 根据ID删除model
     *
     * @param id
     * @return
     */
    public boolean deleteById(Object id);

    /**
     * 删除
     *
     * @param model
     * @return
     */
    public boolean delete(UserRole model);


    /**
     * 保存到数据库
     *
     * @param model
     * @return
     */
    public Object save(UserRole model);

    /**
     * 保存或更新
     *
     * @param model
     * @return
     */
    public Object saveOrUpdate(UserRole model);

    /**
     * 更新 model
     *
     * @param model
     * @return
     */
    public boolean update(UserRole model);

}